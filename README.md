# Spring Basic Tips


## How Spring MVC works?
---
![Alt text](./img/spring-mvc.png)
Notes:
- Controller and View communicates using Model.

<br/>
<br/>


## What is Controller and Services?
---
![Alt text](./img/dependency-injection.png)
Controller is Me and Services are Mommy or Daddy.

<br/>
<br/>

## Dependency Injection into Controller?
---
### 3 ways to implements
1. Property - not recommanded
```java
@Controller
public class PropertyInjectedController {

    @Qualifier("propertyInjectedGreetingService")
    @Autowired
    public GreetingService greetingService;

    public String getGreeting(){
        return greetingService.sayGreeting();
    }
}
```
2. Setter
```java
@Controller
public class SetterInjectedController {

    private GreetingService greetingService;

    @Qualifier("setterInjectedGreetingService")
    @Autowired
    public void setGreetingService(GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting(){
        return greetingService.sayGreeting();
    }
}
```
3. Constructor - recommanded 

@Autowired is not required.

private final was used to ensure not to change the Service in the Controller.

```java
@Controller
public class ConstructorInjectedController {
    private final GreetingService greetingService;

    public ConstructorInjectedController(@Qualifier("constructorGreetingService") GreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getGreeting(){
        return greetingService.sayGreeting();
    }
}
```

<br/>
<br/>

## Profile Services?
---
```
ex)
interface I18nService -> @Profile({"EN","default"}) I18nEnglishService 
                      -> @Profile("ES") I18nSpanishService
```

In the application.properties file

```
spring.profiles.active=EN
```

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

## References
---
https://www.udemy.com/course/spring-framework-5-beginner-to-guru/

